#!/usr/bin/python3

import copy
import math
import random
import re
import ast

class COG(object):

    #ADJA = '+'
    #DIAG = 'x'
    #UP   = '^'
    #DOWN = 'v'
    #LEFT = '<'
    #RIGH = '>'
    #ROW  = '-'
    #COL  = '|'
    #OMNI = '*'
    #SPEC = '#'
    #NORM = ' '

    ADJA = 0
    DIAG = 1
    UP   = 2
    DOWN = 3
    LEFT = 4
    RIGH = 5
    ROW  = 6
    COL  = 7
    OMNI = 8
    SPEC = 9
    NORM = 10
    def __init__(self,name,style,build,flaggy,exp,expspeed=0,boost_build=0,boost_flaggy=0,boost_exp=0):
        self.name         = name
        self.style        = style
        self.build        = build
        self.flaggy       = flaggy
        self.exp          = exp
        self.expspeed     = expspeed
        self.boost_build  = boost_build
        self.boost_flaggy = boost_flaggy
        self.boost_exp    = boost_exp

    def __str__(self):
        if self.style == COG.NORM:
            cogtype = 'Cog'
        elif self.style == COG.ADJA:
            cogtype = 'Plus_Cog'
        elif self.style == COG.DIAG:
            cogtype = 'X_Cog'
        elif self.style == COG.UP:
            cogtype = 'Up_Cog'
        elif self.style == COG.DOWN:
            cogtype = 'Down_Cog'
        elif self.style == COG.LEFT:
            cogtype = 'Left_Cog'
        elif self.style == COG.RIGH:
            cogtype = 'Right_Cog'
        elif self.style == COG.ROW:
            cogtype = 'Row_Cog'
        elif self.style == COG.COL:
            cogtype = 'Col_Cog'
        elif self.style == COG.OMNI:
            cogtype = 'Omni_Cog'
        elif self.style == COG.SPEC:
            cogtype = 'Yang_Cog'

#        return str(cogtype + ',' +
#                   self.name + ',' +
#                   str(self.build) + ',' +
#                   str(self.flaggy) + ',' +
#                   str(self.exp/100) + ',' +
#                   str(self.exp/100) + ',' +
#                   str(self.boost_build/100) + ',' +
#                   str(self.boost_flaggy/100) + ',' +
#                   str(0) + ',' +
#                   str(self.boost_exp/100))
        return str("COG('" + str(self.name) +
                           "'," + str(self.style) + 
                           "," + str(self.build) + 
                           "," + str(self.flaggy) +
                           "," + str(self.exp) +
                           "," + str(self.expspeed) +
                           "," + str(self.boost_build) +
                           "," + str(self.boost_flaggy) +
                           "," + str(self.boost_exp) +
                           ")")
    def __repr__(self):
        return self.__str__()


class BOARD(object):

    BLOCK = 'B'
    FLAG = 'F'
    OPEN = 'O'
    
    WIDTH = 12
    HEIGHT = 8

    def __init__(self,cogshelf = [], cogorder = []):
        self.board = [[BOARD.BLOCK for x in range(BOARD.WIDTH)] for y in range(BOARD.HEIGHT)]
        self.cogshelf = cogshelf
        self.cogorder = cogorder

    def __str__(self):
        line = ''
        for r in range(len(self.board)):
            for c in range(len(self.board[0])):
                if(isinstance(self.board[r][c],COG)):
                    line += "{:<31}".format(str(self.board[r][c]))
            line += '\n'
        return line

    def setboard(self,board):
        self.board = board

    def arrangecogs(self):
        i = 0
        for row in range(len(self.board)):
            for column in range(len(self.board[0])):
                if self.board[row][column] == BOARD.OPEN and i < len(self.cogshelf):
                    self.board[row][column] = self.cogshelf[self.cogorder[i]]
                    i+=1
        


    def createboostboard(self):
        boostboard = BOARD()
        for r in range(len(self.board)):
            for c in range(len(self.board[0])):
                if isinstance(self.board[r][c],COG):
                    boostboard.board[r][c] = COG('Calc Cog'
                                                 ,self.board[r][c].style
                                                 ,0
                                                 ,0
                                                 ,0
                                                 ,0 
                                                 ,self.board[r][c].boost_build
                                                 ,self.board[r][c].boost_flaggy
                                                 ,self.board[r][c].boost_exp)
        return boostboard
    
    def getsolutionboard(self):
        line = ''
        for r in range(len(self.board)):
            for c in range(len(self.board[0])):
                if(isinstance(self.board[r][c],COG)):
                    if self.board[r][c].expspeed:
                        line += ' M'
                    elif self.board[r][c].style == COG.ADJA:
                        line += ' +'
                    elif self.board[r][c].style == COG.DIAG:
                        line += ' X'
                    elif self.board[r][c].style == COG.UP:
                        line += ' ^'
                    elif self.board[r][c].style == COG.DOWN:
                        line += ' V'
                    elif self.board[r][c].style == COG.LEFT:
                        line += ' <'
                    elif self.board[r][c].style == COG.RIGH:
                        line += ' >'
                    elif self.board[r][c].style == COG.ROW:
                        line += ' -'
                    elif self.board[r][c].style == COG.COL:
                        line += ' |'
                    elif self.board[r][c].style == COG.OMNI:
                        line += ' *'
                    elif self.board[r][c].style == COG.SPEC:
                        line += ' Y'
                    elif self.board[r][c].style == COG.NORM:
                        line += ' O'
                else:
                    line += ' E'
            line += '\n'
        return line
        

    def calculate(self):
        boostboard = self.createboostboard()
        for r in range(len(boostboard.board)):
            for c in range(len(boostboard.board[0])):
                if isinstance(boostboard.board[r][c],COG):
                    cog = boostboard.board[r][c]
                    if cog.style == COG.ADJA:
                        boostboard.applyadjacent(r,c)
                    elif cog.style == COG.DIAG:
                        boostboard.applydiagonal(r,c)
                    elif cog.style == COG.UP:
                        boostboard.applyup(r,c)
                    elif cog.style == COG.DOWN:
                        boostboard.applydown(r,c)
                    elif cog.style == COG.LEFT:
                        boostboard.applyleft(r,c)
                    elif cog.style == COG.RIGH:
                        boostboard.applyright(r,c)
                    elif cog.style == COG.ROW:
                        boostboard.applyrow(r,c)
                    elif cog.style == COG.COL:
                        boostboard.applycolumn(r,c)
                    elif cog.style == COG.OMNI:
                        boostboard.applyomni(r,c)
                    elif cog.style == COG.SPEC:
                        boostboard.applyspecial(r,c)
        buildtot    = 0
        flaggytot   = 0
        exptot      = 0
        expspeedtot = 0
        constrxptot = 0
        overalltot  = 0
        for r in range(len(boostboard.board)):
            for c in range(len(boostboard.board[0])):
                if isinstance(boostboard.board[r][c],COG):
                    buildtot    += math.ceil(self.board[r][c].build    * (100 + boostboard.board[r][c].build)   /100)
                    flaggytot   += math.ceil(self.board[r][c].flaggy   * (100 + boostboard.board[r][c].flaggy)  /100)
                    expspeedtot += math.ceil(self.board[r][c].expspeed * (100 + boostboard.board[r][c].expspeed)/100)
                    #if r ==3 and c ==3:
                    #    print((r,c,self.board[r][c].expspeed,boostboard.board[r][c].expspeed))
                    exptot      += math.ceil(self.board[r][c].exp)

        buildtot    = math.ceil(buildtot)
        flaggytot   = math.ceil(flaggytot)
        exptot      = math.ceil(exptot)
        expspeedtot = math.ceil(expspeedtot)
        overalltot  = buildtot+exptot*5+expspeedtot*10

        return (buildtot,flaggytot,exptot,expspeedtot,overalltot)
    
    def applybuff(self,row,column,svert,shori):
        if BOARD.HEIGHT > (row    + svert) >= 0 and BOARD.WIDTH > (column + shori) >= 0 and isinstance(self.board[row+svert][column+shori],COG):
            self.board[row+svert][column+shori].build    += self.board[row][column].boost_build
            self.board[row+svert][column+shori].flaggy   += self.board[row][column].boost_flaggy
            self.board[row+svert][column+shori].expspeed += self.board[row][column].boost_exp

    def applyadjacent(self,row,column):
        self.applybuff(row,column,-1, 0)
        self.applybuff(row,column, 1, 0)
        self.applybuff(row,column, 0,-1)
        self.applybuff(row,column, 0, 1)

    def applydiagonal(self,row,column):
        self.applybuff(row,column,-1,-1)
        self.applybuff(row,column,-1,1)
        self.applybuff(row,column,1,-1)
        self.applybuff(row,column,1,1)

    def applyup(self,row,column):
        self.applybuff(row,column,-1,-1)
        self.applybuff(row,column,-1,0)
        self.applybuff(row,column,-1,1)
        self.applybuff(row,column,-2,-1)
        self.applybuff(row,column,-2,0)
        self.applybuff(row,column,-2,1)

    def applydown(self,row,column):
        self.applybuff(row,column,1,-1)
        self.applybuff(row,column,1,0)
        self.applybuff(row,column,1,1)
        self.applybuff(row,column,2,-1)
        self.applybuff(row,column,2,0)
        self.applybuff(row,column,2,1)

    def applyleft(self,row,column):
        self.applybuff(row,column,-1,-1)
        self.applybuff(row,column, 0,-1)
        self.applybuff(row,column, 1,-1)
        self.applybuff(row,column,-1,-2)
        self.applybuff(row,column, 0,-2)
        self.applybuff(row,column, 1,-2)

    def applyright(self,row,column):
        self.applybuff(row,column,-1,1)
        self.applybuff(row,column, 0,1)
        self.applybuff(row,column, 1,1)
        self.applybuff(row,column,-1,2)
        self.applybuff(row,column, 0,2)
        self.applybuff(row,column, 1,2)


    def applyrow(self,row,column):
        for x in range(-column,BOARD.WIDTH-column,1):
            if x != 0:                                                       
                self.applybuff(row,column,0,x)

    def applycolumn(self,row,column):
        for y in range(-row,BOARD.HEIGHT-row,1):
            if y != 0:                                                       
                self.applybuff(row,column,y,0)

    def applyomni(self,row,column):
        self.applybuff(row,column,-2,-2)
        self.applybuff(row,column,-2,2)
        self.applybuff(row,column,2,-2)
        self.applybuff(row,column,2,2)

    def applyspecial(self,row,column):
        self.applybuff(row,column,-1,-1)
        self.applybuff(row,column,-1,0)
        self.applybuff(row,column,-1,1)
        self.applybuff(row,column,0,-1)
        self.applybuff(row,column,0,1)
        self.applybuff(row,column,1,-1)
        self.applybuff(row,column,1,0)
        self.applybuff(row,column,1,1)
        self.applybuff(row,column,-2,0)
        self.applybuff(row,column,2,0)
        self.applybuff(row,column,0,-2)
        self.applybuff(row,column,0,2)

BUILD = 0
FLAGGY = 1
EXP = 2
EXPSPEED = 3
EXPSPEEDBUILD = 4

def getindex(lis, value):
    try:
        index = lis.index(value)
    except ValueError:
        index = -1
    return index



def findcycle(parent1, parent2, start = 0, cycles = [], group = 0):
    index  = start
    while True:
        cycles[index] = group
        index  = getindex(parent1,parent2[index])
        if index == -1:
            print('ERROR Value: ', parent2[index], ' not found.')
            break
        if  start == index or index == -1:
            break

    return cycles
    

def findallcycles(parent1,parent2):
    assert(len(parent1) == len(parent2))
    cycles = ['X'] * len(parent1)
    start = 0
    group = 0
    while getindex(cycles,'X') != -1:
        findcycle(parent1,parent2,start,cycles,group)
        group += 1
        start = getindex(cycles,'X')
    return cycles
    
def crossover(parent1,parent2,cycles):
    child1 = ['X'] * len(parent1)
    child2 = ['X'] * len(parent2)
    for i in range(len(parent1)):
        if cycles[i] % 2 == 0:
            child1[i] = parent1[i]
            child2[i] = parent2[i]
        else:
            child1[i] = parent2[i]
            child2[i] = parent1[i]

    return (child1,child2)            

def mutation(child):
    if random.randint(1,100) > 30:
        index1 = random.randint(0,len(child)-1)
        index2 = random.randint(0,len(child)-1)
        temp = 0
        temp = child[index1]
        child[index1] = child[index2]
        child[index2] = temp
    
    return child

def rungeneration(solutions,board):
    #we ignore parent3 onwards, they are too weak.    
    parent1 = solutions[0][1]
    parent2 = solutions[1][1]
    
    child1 = BOARD(parent1.cogshelf)
    child1.setboard(copy.deepcopy(board))
    
    child2 = BOARD(parent1.cogshelf)
    child2.setboard(copy.deepcopy(board))
    
    child3 = BOARD(parent1.cogshelf)
    child3.setboard(copy.deepcopy(board))
    
    child4 = BOARD(parent1.cogshelf)
    child4.setboard(copy.deepcopy(board))
    
    (child1.cogorder,child3.cogorder) = crossover(parent1.cogorder,parent2.cogorder,findallcycles(parent1.cogorder,parent2.cogorder))
    #(child2.cogorder,child4.cogorder) = crossover(parent3.cogorder,parent4.cogorder,findallcycles(parent3.cogorder,parent4.cogorder))
    #Child of parents
    child1.cogorder = mutation(child1.cogorder)
    child1.arrangecogs()
    #Only a variation of one of the parents
    child2.cogorder = mutation(parent1.cogorder)
    child2.arrangecogs()
    #Child of the parents
    child3.cogorder = mutation(child3.cogorder)
    child3.arrangecogs()
    #we introduce randomness into the children
    child4.cogorder = mutation(parent2.cogorder)
    random.shuffle(child4.cogorder)
    child4.arrangecogs()

    return [(child1.calculate(),child1),(child2.calculate(),child2),(child3.calculate(),child3),(child4.calculate(), child4)]

def evolve(shelf,board,ctype = BUILD):
    print('Setting up initial solutions')
    startord = list(range(len(shelf)))
    #we dont shuffle it here because if we have a best solution so far we want to improve over it.
    #random.shuffle(startord)
    solution1 = BOARD(shelf,copy.deepcopy(startord))
    solution1.setboard(copy.deepcopy(board))
    solution1.arrangecogs()
    #print(solution1.board[3][3])
    
    random.shuffle(startord)
    solution2 = BOARD(shelf,copy.deepcopy(startord))
    solution2.setboard(copy.deepcopy(board))
    solution2.arrangecogs()

    random.shuffle(startord)
    solution3 = BOARD(shelf,copy.deepcopy(startord))    
    solution3.setboard(copy.deepcopy(board))
    solution3.arrangecogs()

    random.shuffle(startord)
    solution4 = BOARD(shelf,copy.deepcopy(startord))
    solution4.setboard(copy.deepcopy(board))
    solution4.arrangecogs()
    bestsolution = copy.deepcopy(solution1)

    print('Evolving solution...')
    count = 0
    generation = 0
    solutions = [(solution1.calculate(),solution1),(solution2.calculate(),solution2),(solution3.calculate(),solution3),(solution4.calculate(),solution4)]
    while count < 100000:
        if generation % 10000 == 0:
            print("Generation: {}".format(generation))
        solutions.sort(key=lambda x: x[0][ctype], reverse=True)
        if solutions[0][0][ctype] > bestsolution.calculate()[ctype]:
            bestsolution = copy.deepcopy(solutions[0][1])
            count = 0
            print('Better solution found: ' + str(bestsolution.calculate()))
            print(bestsolution.getsolutionboard())
        solutions = rungeneration(solutions,board)
        count += 1
        generation += 1
    print('--------Characteristics---------')
    print('Build: %d Flaggy: %d Exp: %d Player exp %d Overall %d' % bestsolution.calculate())
    print('----------Board---------')
    print(bestsolution.getsolutionboard())
    print(bestsolution)
    print('Serializing Output. This can replace the current cogshelf in the code to start from this solution.')
    print([bestsolution.cogshelf[i] for i in bestsolution.cogorder])

def parsecogdata(string):
    cogshelf = []
    if not re.match(r"[^\",0-9a-z{}:\\]",string):
        cogs = ast.literal_eval(string)
        #print("Number of items {}".format(len(cogs.keys())))
        for key in cogs.keys():
            if key not in ('96','97','98','99','100','101','102','103','104','105','106','107'):
                buildrate  = cogs[key]['a'] if 'a' in cogs[key] else 0
                flagrate   = cogs[key]['c'] if 'c' in cogs[key] else 0
                expspeed   = cogs[key]['b'] if 'b' in cogs[key] else 0
                exprate    = cogs[key]['d'] if 'd' in cogs[key] else 0
                boostbuild = cogs[key]['e'] if 'e' in cogs[key] else 0
                boostflag  = cogs[key]['g'] if 'g' in cogs[key] else 0
                boostexp   = cogs[key]['f'] if 'f' in cogs[key] else 0
                cogtype = COG.NORM
                if 'h' in cogs[key]:
                    if cogs[key]['h'] == 'up':
                        cogtype = COG.UP
                    elif cogs[key]['h'] == 'down':
                        cogtype = COG.DOWN
                    elif cogs[key]['h'] == 'right':
                        cogtype = COG.RIGH
                    elif cogs[key]['h'] == 'left':
                        cogtype = COG.LEFT
                    elif cogs[key]['h'] == 'column':
                        cogtype = COG.COL
                    elif cogs[key]['h'] == 'row':
                        cogtype = COG.ROW
                    elif cogs[key]['h'] == 'diagonal':
                        cogtype = COG.DIAG
                    elif cogs[key]['h'] == 'adjacent':
                        cogtype = COG.ADJA
                    elif cogs[key]['h'] == 'corners':
                        cogtype = COG.OMNI
                    elif cogs[key]['h'] == 'around':
                        cogtype = COG.SPEC
                cogshelf.append(COG('Cog',cogtype,buildrate,flagrate,exprate,expspeed,boostbuild,boostflag,boostexp))
                #cogshelf.reverse()
    else:
        print("Invalid cog data provided")

    print("Number in cogshelf {}".format(len(cogshelf)))
    return cogshelf



def expsort(cog):
    if cog.build > 90:
        result = cog.build
    else:
        result = cog.exp
    return result
def string_escape(s, encoding='utf-8'):
    return (s.encode('latin1')         # To bytes, required by 'unicode-escape'
             .decode('unicode-escape') # Perform the actual octal-escaping decode
             .encode('latin1')         # 1:1 mapping back to bytes
             .decode(encoding))        # Decode original encoding

def main(evolutiontype = EXPSPEEDBUILD):
    currentboard = [['B','O','O','O','O','O','O','O','O','O','O','B'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['O','O','O','O','O','O','O','O','O','O','O','O'],
                    ['B','O','O','O','O','O','O','O','O','O','O','B']]
 
    #spaces = sum([1 for x in currentboard for y in x if y == 'O'])
    #parse the cog data 
    with open('cogdata.txt','r') as f:
        cogdata = string_escape(f.readline())
    #print(cogdata)
    cogshelf = parsecogdata(cogdata)
    #evolve the solution
    evolve(cogshelf,currentboard,evolutiontype)

if __name__=="__main__":
    main()
