# Cog Calculator

## Description
This library allows the user to optimize the arrangement of the cogs in IdleOn. It uses a genetic algorithm to evolve a better arrangement of the cogs. To optimize different characteristics the user can select what values to optimize for. The program will show one of the best possible arrangements of cogs after 100 000 tries with no better solution.

## Installation
This program doesn't require to install any library and works with basic 3

## Execution
As of now this program only requires the json of the cogs of the player and which values should be prioritized when evolving the solution.

The first step is to replace the content of cogdata.txt with the json of the cogs you have. You can find this on [Idleon Efficiency](https://www.idleonefficiency.com/raw-data). You only need to find the section that starts with the word CogM.

For example: "CogM": "{\\"1\\"...\\50}}",

You need to get the information from the first " to the last " without the double quoation marks. 

```
{\"1\"...":50}}
``` 

```
$ ./cog_calculator.py
```
## Example Output
```
$ ./cog_calculator.py 
Number in cogshelf 109
Setting up initial solutions
Evolving solution...
Generation: 0
Better solution found: (535834, 24221, 3966, 6395272, 64508384)
 E O O | O O O O > O O E
 O O V O O O O O O + O O
 O > X V Y O O O O X O O
 O Y > M < - - - - - O -
 O O > + Y O O O O O O O
 O O O | ^ * O O O O O O
 O O O | O O O O O O O O
 E O + - O O O O O O O E

.
.
.

Generation: 100000
--------Characteristics---------
Build: 536722 Flaggy: 24238 Exp: 3945 Player exp 6395272 Overall 64509167
----------Board---------
 E O O | O O O O O O O E
 O O V O O O O O O + O O
 O > X V Y O O O O X O O
 O Y > M < - - - - - O -
 O O > + Y O O O O O O O
 O O O | ^ * O O O O O O
 O O O | O O O O O O O O
 E O O - O O O O O > O E

```

## Future Improvements
Make the locations of the cogs easier to see.

## License
GNU General Public License v3.0
